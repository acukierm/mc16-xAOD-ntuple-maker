// C++ stuff
#include <vector>

// Root stuff
#include <TTree.h>

// Basic Analysis set-ups
#include <AsgTools/MessageCheck.h>
#include <NtupleMaker/NtupleMakerAna.h>

// xAOD:  Event-level info. and meta data
#include <xAODEventInfo/EventInfo.h>
// xAOD: Jet stuff
#include <xAODJet/JetContainer.h>


#define ARRAY_INIT {}


NtupleMakerAna :: NtupleMakerAna (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
      m_jetCleaning ("JetCleaningTool/JetCleaning", this),
      m_JERTool ("JERTool", this),
      m_JetCalibrationTool ("JetCalibrationTool", this),
      // variables to read out data
      m_jetE (ARRAY_INIT),
      m_jetPt (ARRAY_INIT),
      m_jetEta (ARRAY_INIT),
      m_jetPhi (ARRAY_INIT),
      m_jetM (ARRAY_INIT),
      m_jetTiming (ARRAY_INIT),
      m_jetNegE (ARRAY_INIT),
      m_jetCharge (ARRAY_INIT),
      m_jetAngularity (ARRAY_INIT),
      m_jetFlavor (ARRAY_INIT),
      m_largeRjetE (ARRAY_INIT),
      m_largeRjetPt (ARRAY_INIT),
      m_largeRjetEta (ARRAY_INIT),
      m_largeRjetPhi (ARRAY_INIT),
      m_largeRjetM (ARRAY_INIT),
      m_largeRjetFlavor (ARRAY_INIT),
      m_largeRjetD2 (ARRAY_INIT),
      m_largeRjetE_uncal (ARRAY_INIT),
      m_largeRjetPt_uncal (ARRAY_INIT),
      m_largeRjetEta_uncal (ARRAY_INIT),
      m_largeRjetPhi_uncal (ARRAY_INIT),
      m_largeRjetM_uncal (ARRAY_INIT),
      m_largeRtruthjetE (ARRAY_INIT),
      m_largeRtruthjetPt (ARRAY_INIT),
      m_largeRtruthjetEta (ARRAY_INIT),
      m_largeRtruthjetPhi (ARRAY_INIT),
      m_largeRtruthjetM (ARRAY_INIT),
      m_largeRtruthjetFlavor (ARRAY_INIT)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.
  declareProperty("Verbose", m_verbose = false, "Verbose output");
  declareProperty("doLargeRJets", m_doLargeRJets = false, "Do large R jets output");
  declareProperty("doTruth", m_doTruth = false, "Do truth jets output");
  declareProperty("overallWeight", m_overallWeight = 1., "MC norm weight");
}



NtupleMakerAna::~NtupleMakerAna (){
  if (m_jetE)   delete m_jetE;
  if (m_jetPt)  delete m_jetPt;
  if (m_jetEta) delete m_jetEta;
  if (m_jetPhi) delete m_jetPhi;
  if (m_jetM) delete m_jetM;
  if (m_jetTiming) delete m_jetTiming;
  if (m_jetNegE) delete m_jetNegE;
  if (m_jetCharge) delete m_jetCharge;
  if (m_jetAngularity) delete m_jetAngularity;
  if (m_jetFlavor) delete m_jetFlavor;
  if (m_largeRjetE)   delete m_largeRjetE;
  if (m_largeRjetPt)  delete m_largeRjetPt;
  if (m_largeRjetEta) delete m_largeRjetEta;
  if (m_largeRjetPhi) delete m_largeRjetPhi;
  if (m_largeRjetM) delete m_largeRjetM;
  if (m_largeRjetFlavor) delete m_largeRjetFlavor;
  if (m_largeRjetD2) delete m_largeRjetD2;
  if (m_largeRjetE_uncal)   delete m_largeRjetE_uncal;
  if (m_largeRjetPt_uncal)  delete m_largeRjetPt_uncal;
  if (m_largeRjetEta_uncal) delete m_largeRjetEta_uncal;
  if (m_largeRjetPhi_uncal) delete m_largeRjetPhi_uncal;
  if (m_largeRjetM_uncal) delete m_largeRjetM_uncal;
  if (m_largeRtruthjetE)   delete m_largeRtruthjetE;
  if (m_largeRtruthjetPt)  delete m_largeRtruthjetPt;
  if (m_largeRtruthjetEta) delete m_largeRtruthjetEta;
  if (m_largeRtruthjetPhi) delete m_largeRtruthjetPhi;
  if (m_largeRtruthjetM) delete m_largeRtruthjetM;
  if (m_largeRtruthjetFlavor) delete m_largeRtruthjetFlavor;
}



StatusCode NtupleMakerAna :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TTree ("ntuple", "ntuple tree")));
  TTree* mytree = tree ("ntuple");
  // Branches in ntuple tree
  mytree->Branch ("RunNumber",        &m_runNumber,   "RunNumber/I");
  mytree->Branch ("EventNumber",      &m_eventNumber, "EventNumber/I");
  mytree->Branch ("EventWeight",      &m_eventWeight, "EventWeight/F");
  // Small R Reco. jet
  mytree->Branch ("JetE",             "std::vector<float>",      &m_jetE);
  mytree->Branch ("JetPt",            "std::vector<float>",      &m_jetPt);
  mytree->Branch ("JetEta",           "std::vector<float>",      &m_jetEta);
  mytree->Branch ("JetPhi",           "std::vector<float>",      &m_jetPhi);
  mytree->Branch ("JetM",             "std::vector<float>",      &m_jetM);
  mytree->Branch ("JetTiming",        "std::vector<float>",      &m_jetTiming);
  mytree->Branch ("JetNegE",          "std::vector<float>",      &m_jetNegE);
  mytree->Branch ("JetCharge",        "std::vector<float>",      &m_jetCharge);
  mytree->Branch ("JetAngularity",    "std::vector<float>",      &m_jetAngularity);
  mytree->Branch ("JetFlavor",         "std::vector<int>",     &m_jetFlavor);

  // Large R Reco. jet
  if(m_doLargeRJets){
    mytree->Branch ("LargeRJetE",             "std::vector<float>",      &m_largeRjetE);
    mytree->Branch ("LargeRJetPt",            "std::vector<float>",      &m_largeRjetPt);
    mytree->Branch ("LargeRJetEta",           "std::vector<float>",      &m_largeRjetEta);
    mytree->Branch ("LargeRJetPhi",           "std::vector<float>",      &m_largeRjetPhi);
    mytree->Branch ("LargeRJetM",             "std::vector<float>",      &m_largeRjetM);
    mytree->Branch ("LargeRJetFlavor",         "std::vector<int>",     &m_largeRjetFlavor);
    mytree->Branch ("LargeRJetD2",         "std::vector<float>",     &m_largeRjetD2);
    mytree->Branch ("LargeRJetE_uncal",             "std::vector<float>",      &m_largeRjetE_uncal);
    mytree->Branch ("LargeRJetPt_uncal",            "std::vector<float>",      &m_largeRjetPt_uncal);
    mytree->Branch ("LargeRJetEta_uncal",           "std::vector<float>",      &m_largeRjetEta_uncal);
    mytree->Branch ("LargeRJetPhi_uncal",           "std::vector<float>",      &m_largeRjetPhi_uncal);
    mytree->Branch ("LargeRJetM_uncal",             "std::vector<float>",      &m_largeRjetM_uncal);
    if(m_doTruth){
      mytree->Branch ("LargeRTruthJetE",             "std::vector<float>",      &m_largeRtruthjetE);
      mytree->Branch ("LargeRTruthJetPt",            "std::vector<float>",      &m_largeRtruthjetPt);
      mytree->Branch ("LargeRTruthJetEta",           "std::vector<float>",      &m_largeRtruthjetEta);
      mytree->Branch ("LargeRTruthJetPhi",           "std::vector<float>",      &m_largeRtruthjetPhi);
      mytree->Branch ("LargeRTruthJetM",             "std::vector<float>",      &m_largeRtruthjetM);
      mytree->Branch ("LargeRTruthJetFlavor",         "std::vector<int>",     &m_largeRtruthjetFlavor);
    }
  }

  // Initialize and configure the jet cleaning tool
  ANA_CHECK (m_jetCleaning.setProperty( "CutLevel", "LooseBad"));
  ANA_CHECK (m_jetCleaning.setProperty("DoUgly", false));
  ANA_CHECK (m_jetCleaning.initialize());

  // Initialize the JER tool using default configurations
  ANA_CHECK (m_JERTool.initialize());

  // Initialize the Jet calibration tool using Large R Jet configurations
  TString jetAlgo    = "AntiKt10LCTopoTrimmedPtFrac5SmallR20";  // Jet collection, for example AntiKt4EMTopo or AntiKt4LCTopo (see below)
  TString config     = "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config";  // Global config (see below)
  TString calibSeq  = "EtaJES_JMS"; // Calibration sequence to apply (see below)
  TString calibArea = "00-04-82"; // Calibration Area tag (see below)
  bool isData          = false; // bool describing if the events are data or from simulation

  ANA_CHECK( m_JetCalibrationTool.setProperty("JetCollection",jetAlgo.Data()) );
  ANA_CHECK( m_JetCalibrationTool.setProperty("ConfigFile",config.Data()) );
  ANA_CHECK( m_JetCalibrationTool.setProperty("CalibSequence",calibSeq.Data()) );
  ANA_CHECK( m_JetCalibrationTool.setProperty("CalibArea",calibArea.Data()) );
  ANA_CHECK( m_JetCalibrationTool.setProperty("IsData",isData) );
  ANA_CHECK( m_JetCalibrationTool.retrieve() );

  return StatusCode::SUCCESS;
}



StatusCode NtupleMakerAna :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  // retrieve the eventInfo object from the event store
  // and the event/run numbers from the eventInfo object
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  
  bool isMC = false;
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true;
  }

  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  if(isMC){
    // data has weight 1
    m_eventWeight = eventInfo->mcEventWeight(0)*m_overallWeight; //*lumi to get # of events
  }
  // print out run and event number from retrieved object
  if(m_verbose) ANA_MSG_INFO ( "EXECUTE: runNumber = " << m_runNumber << ", eventNumber = " << m_eventNumber );


  // BEGIN jet stuff
  // get jet container of interest
  const xAOD::JetContainer* jets = 0;
  ANA_CHECK (evtStore()->retrieve( jets, "AntiKt4EMTopoJets"));
  // jet cleaning
  unsigned int numGoodJets = 0;
  if(m_verbose) ANA_MSG_INFO ("EXECUTE: number of reco. jets = " << jets->size());

  // Clear the jet vectors for each event
  m_jetE->clear();
  m_jetPt->clear();
  m_jetEta->clear();
  m_jetPhi->clear();
  m_jetM->clear();
  m_jetTiming->clear();
  m_jetNegE->clear();
  m_jetCharge->clear();
  m_jetAngularity->clear();
  m_jetFlavor->clear();

  // Placeholder variables to retrieve jet variables
  float j_Timing = 0;
  float j_NegE = 0;
  float j_Charge = 0;
  float j_Angularity = 0;
  int j_Flavor = 0;

  for (const xAOD::Jet* jet : *jets) {
    if (!m_jetCleaning->keep (*jet)) continue; // only keep good clean jets
    ++numGoodJets;

    if (isMC) {
      // get the jet energy MC resolution
      // double mcRes = m_JERTool->getRelResolutionMC(jet);
      // get the resolution uncertainty
      // double mcRes_uncert = m_JERTool->getUncertainty(jet);

      // For now, we don't do anything with these MC smearing...

      jet->getAttribute<int>("PartonTruthLabelID", j_Flavor);
      m_jetFlavor->push_back (j_Flavor); 
    }

    // retrieve the most basic variables
    m_jetE->push_back (jet->e() / 1000.);
    m_jetPt->push_back (jet->pt() / 1000.);
    m_jetEta->push_back (jet->eta());
    m_jetPhi->push_back (jet->phi());
    m_jetM->push_back (jet->m() / 1000.);
    jet->getAttribute("Timing", j_Timing);
    m_jetTiming->push_back (j_Timing);
    jet->getAttribute("NegativeE", j_NegE);
    m_jetNegE->push_back (j_NegE);
    jet->getAttribute("Charge", j_Charge);
    m_jetCharge->push_back (j_Charge);
    jet->getAttribute("Angularity", j_Angularity);
    m_jetAngularity->push_back (j_Angularity);
    
  }
  if(m_verbose) ANA_MSG_INFO ("EXECUTE: number of good clean reco. jets = " << numGoodJets);
  // END jet stuff

  // BEGIN Large R jet stuff
  if (m_doLargeRJets){
    // get jet container of interest
    const xAOD::JetContainer* largeRjets = 0;
    ANA_CHECK (evtStore()->retrieve( largeRjets, "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"));

    // Clear the jet vectors for each event
    m_largeRjetE->clear();
    m_largeRjetPt->clear();
    m_largeRjetEta->clear();
    m_largeRjetPhi->clear();
    m_largeRjetM->clear();
    m_largeRjetFlavor->clear();
    m_largeRjetD2->clear();

    m_largeRjetE_uncal->clear();
    m_largeRjetPt_uncal->clear();
    m_largeRjetEta_uncal->clear();
    m_largeRjetPhi_uncal->clear();
    m_largeRjetM_uncal->clear();

    // Placeholder variables to retrieve jet variables
    int j_Flavor = 0;

    for (const xAOD::Jet* jet : *largeRjets) {
      xAOD::Jet * cjet = 0;

      m_JetCalibrationTool->calibratedCopy(*jet,cjet); //calibrated copy

      if (cjet->pt()/1000.<50) continue;
      //if (abs(cjet->eta())>2.0) continue;

      if (isMC) {
        //Truth info
        cjet->getAttribute<int>("PartonTruthLabelID", j_Flavor);
        m_largeRjetFlavor->push_back (j_Flavor); 
      }

      // retrieve the most basic variables
      m_largeRjetE->push_back (cjet->e() / 1000.);
      m_largeRjetPt->push_back (cjet->pt() / 1000.);
      m_largeRjetEta->push_back (cjet->eta());
      m_largeRjetPhi->push_back (cjet->phi());
      m_largeRjetM->push_back (cjet->m() / 1000.);

      //std::cout << "D2: " << D2(*cjet) << ";" << D2(*jet) << std::endl; //same
      m_largeRjetD2->push_back (D2(*cjet)); 

      // retrieve the uncalibrated variables
      m_largeRjetE_uncal->push_back (jet->e() / 1000.);
      m_largeRjetPt_uncal->push_back (jet->pt() / 1000.);
      m_largeRjetEta_uncal->push_back (jet->eta());
      m_largeRjetPhi_uncal->push_back (jet->phi());
      m_largeRjetM_uncal->push_back (jet->m() / 1000.);

      delete cjet;
    } //END reco jets

    if (m_doTruth && isMC){
      // get truth jet container of interest
      const xAOD::JetContainer* largeRtruthjets = 0;
      ANA_CHECK (evtStore()->retrieve( largeRtruthjets, "AntiKt10TruthTrimmedPtFrac5SmallR20Jets"));

      // Clear the jet vectors for each event
      m_largeRtruthjetE->clear();
      m_largeRtruthjetPt->clear();
      m_largeRtruthjetEta->clear();
      m_largeRtruthjetPhi->clear();
      m_largeRtruthjetM->clear();
      m_largeRtruthjetFlavor->clear();

      // Placeholder variables to retrieve jet variables
      int j_Flavor = 0;
      for (const xAOD::Jet* jet : *largeRtruthjets) {
        jet->getAttribute<int>("PartonTruthLabelID", j_Flavor); //always 0 for truth jets
        m_largeRtruthjetFlavor->push_back (j_Flavor); 

        // retrieve the most basic variables
        m_largeRtruthjetE->push_back (jet->e() / 1000.);
        m_largeRtruthjetPt->push_back (jet->pt() / 1000.);
        m_largeRtruthjetEta->push_back (jet->eta());
        m_largeRtruthjetPhi->push_back (jet->phi());
        m_largeRtruthjetM->push_back (jet->m() / 1000.);
      }
    } //END large R truth jet stuff
  } //END large R jet stuff

  // Fill the event into the tree:
  tree ("ntuple")->Fill ();

  return StatusCode::SUCCESS;
}



StatusCode NtupleMakerAna :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

float NtupleMakerAna::D2(const xAOD::Jet& jet) const{
  static SG::AuxElement::Accessor<float> accECF1("ECF1");
  static SG::AuxElement::Accessor<float> accECF2("ECF2");
  static SG::AuxElement::Accessor<float> accECF3("ECF3");
  // D2
  float D2_reco = -999.0;
  if (accECF1.isAvailable(jet)  and accECF2.isAvailable(jet)  and accECF3.isAvailable(jet)) {
    if(fabs(accECF2(jet)) > 1e-8) // Prevent div-0
      D2_reco = accECF3(jet) * pow(accECF1(jet), 3.0) / pow(accECF2(jet), 3.0);
  }
  return D2_reco;
}
