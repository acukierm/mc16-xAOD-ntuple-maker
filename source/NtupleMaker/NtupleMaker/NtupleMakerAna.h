#ifndef NtupleMaker_NtupleMakerAna_H
#define NtupleMaker_NtupleMakerAna_H

// C++ stuff
#include <vector>

// Root stuff
#include <TTree.h>

// Basic analysis set-ups
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>

// Jet stuff
#include <JetInterface/IJetSelector.h>
#include <JetResolution/IJERTool.h>
#include "JetCalibTools/IJetCalibrationTool.h"

class NtupleMakerAna : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor and destructor
  NtupleMakerAna (const std::string& name, ISvcLocator* pSvcLocator);
  ~NtupleMakerAna ();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  // Analysis-specific functions
  float D2(const xAOD::Jet& jet) const;

private:
  // Configuration, and any other types of variables go here.

  // Flags for the analysis:
  bool m_verbose;
  bool m_doLargeRJets;
  bool m_doTruth;
  float m_overallWeight;

  // Analysis and processing tools
  asg::AnaToolHandle<IJetSelector> m_jetCleaning; //!
  asg::AnaToolHandle<IJERTool> m_JERTool; //!
  asg::AnaToolHandle<IJetCalibrationTool> m_JetCalibrationTool; //!

  // Variables used during ntuple creation
  // Event-level
  unsigned int m_runNumber = 0;              // Run number for the current event
  unsigned long long m_eventNumber = 0;      // Event number
  float m_eventWeight = 1.; //Event weight
  /// Small R Jet variables
  std::vector<float> *m_jetE; //! 
  std::vector<float> *m_jetPt; //!
  std::vector<float> *m_jetEta; //!
  std::vector<float> *m_jetPhi; //!
  std::vector<float> *m_jetM; //!
  std::vector<float> *m_jetTiming; //!
  std::vector<float> *m_jetNegE; //!
  std::vector<float> *m_jetCharge; //!
  std::vector<float> *m_jetAngularity; //!
  std::vector<int> *m_jetFlavor; //!

  /// Large R Jet variables
  std::vector<float> *m_largeRjetE; //! 
  std::vector<float> *m_largeRjetPt; //!
  std::vector<float> *m_largeRjetEta; //!
  std::vector<float> *m_largeRjetPhi; //!
  std::vector<float> *m_largeRjetM; //!
  std::vector<int> *m_largeRjetFlavor; //!
  std::vector<float> *m_largeRjetD2; //!
  /// Large R Jet variables (uncalibrated)
  std::vector<float> *m_largeRjetE_uncal; //! 
  std::vector<float> *m_largeRjetPt_uncal; //!
  std::vector<float> *m_largeRjetEta_uncal; //!
  std::vector<float> *m_largeRjetPhi_uncal; //!
  std::vector<float> *m_largeRjetM_uncal; //!
  /// Large R Truth Jet variables
  std::vector<float> *m_largeRtruthjetE; //! 
  std::vector<float> *m_largeRtruthjetPt; //!
  std::vector<float> *m_largeRtruthjetEta; //!
  std::vector<float> *m_largeRtruthjetPhi; //!
  std::vector<float> *m_largeRtruthjetM; //!
  std::vector<int> *m_largeRtruthjetFlavor; //!
};

#endif
