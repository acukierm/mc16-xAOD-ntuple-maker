# Adding a new variable to the ntuple-maker

## Variable names

e.g.:

* `jete`
  * Reco. jet energy
* `jetpt`
  * Reco. jet $p_T$
* `jeteta`
  * Reco. jet $\eta$
* `jetphi`
  * Reco. jet $\phi$
* `jettime`
  * Reco. jet time

## Options / Specifications, if any

e.g.:

We will stick to anti-kt, $R=0.4$ jets for now, but maybe add other options later. By default, we will keep variables calibrated (e.g. $p_T$, energy, etc.), but maybe we'll add uncalibrated ones later, something like `jetpt-raw`.
